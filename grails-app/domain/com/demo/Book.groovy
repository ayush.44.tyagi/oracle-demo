package com.demo

class Book {

    String name
    String title
    String authorName

    Date dateCreated
    Date lastUpdated

    static constraints = {
        title nullable: true
        authorName nullable: true
    }
}
