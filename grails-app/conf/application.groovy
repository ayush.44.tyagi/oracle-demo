
dataSource {

    driverClassName = "oracle.jdbc.OracleDriver"
    dialect = 'org.hibernate.dialect.Oracle10gDialect'
//    driverClassName = "com.mysql.jdbc.Driver"
//    dialect = 'org.hibernate.dialect.MySQL5InnoDBDialect'
}
hibernate {
    jdbc {
        use_get_generated_keys = true
    }
}

dataSource {
    pooled = true
    dbCreate = "none"
    jmxExport = true
    url = "jdbc:oracle:thin:@saststodb01.integreat.perfios.com:1521:cdb1"
    username = "sec_admin"
    logSql = true
    password = "falcon123!"
//    url = "jdbc:mysql://localhost:3306/book_demo?autoreconnect=true&allowMultiQueries=true&useUnicode=yes&characterEncoding=UTF-8"
//    username = "root"
//    logSql = true
//    password = "nextdefault"

}

grails.plugin.databasemigration.changelogFileName = 'changelog.groovy'
grails.plugin.databasemigration.updateOnStart = true
grails.plugin.databasemigration.updateOnStartFileNames = 'changelog.groovy'
